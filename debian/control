Source: python-pook
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all,
               python3-setuptools,
               python3-hatchling,
               python3-httpx,
               python3-furl,
               python3-xmltodict,
               python3-jsonschema,
               python3-pytest <!nocheck>,
               python3-urllib3 <!nocheck>,
               python3-requests <!nocheck>,
               python3-aiohttp <!nocheck>,
               python3-pytest-cov <!nocheck>,
               python3-pytest-httpbin <!nocheck>,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>
Standards-Version: 4.6.2
Homepage: https://github.com/h2non/pook
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-pook
Vcs-Git: https://salsa.debian.org/python-team/packages/python-pook.git

Package: python3-pook
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-pook-doc
Description: HTTP traffic mocking and testing made easy (Python 3)
 Versatile, expressive and hackable utility library for HTTP traffic mocking
 and expectations made easy in Python. Heavily inspired by gock.
 .
 Features:
   - Simple, expressive and fluent API.
   - Provides both Pythonic and chainable DSL API styles.
   - Full-featured HTTP response definitions and expectations.
   - Matches any HTTP protocol primitive (URL, method, query params, headers,
     body...).
   - Full regular expressions capable mock expectations matching.
   - Supports most popular HTTP clients via interceptor adapters.
   - Configurable volatile, persistent or TTL limited mocks.
   - Works with any testing framework/engine (unittest, pytest...).
   - First-class JSON & XML support matching and responses.
   - Supports JSON Schema body matching.
   - Works in both runtime and testing environments.
   - Can be used as decorator and/or via context managers.
   - Supports real networking mode with optional traffic filtering.
   - Map/filter mocks easily for generic or custom mock expectations.
   - Custom user-defined mock matcher functions.
   - Simulated raised error exceptions.
   - Network delay simulation (only available for aiohttp).
   - Pluggable and hackable API.
   - Customizable HTTP traffic mock interceptor engine.
   - Supports third-party mocking engines, such as mocket.
   - Fits good for painless test doubles.
   - Does not support WebSocket traffic mocking.
   - Works with Python +2.7 and +3.0 (including PyPy).
   - Dependency-less: just 2 small dependencies for JSONSchema and XML tree
     comparison.
 .
 This package installs the library for Python 3.

Package: python-pook-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: HTTP traffic mocking and testing made easy (common documentation)
 Versatile, expressive and hackable utility library for HTTP traffic mocking
 and expectations made easy in Python. Heavily inspired by gock.
 .
 Features:
   - Simple, expressive and fluent API.
   - Provides both Pythonic and chainable DSL API styles.
   - Full-featured HTTP response definitions and expectations.
   - Matches any HTTP protocol primitive (URL, method, query params, headers,
     body...).
   - Full regular expressions capable mock expectations matching.
   - Supports most popular HTTP clients via interceptor adapters.
   - Configurable volatile, persistent or TTL limited mocks.
   - Works with any testing framework/engine (unittest, pytest...).
   - First-class JSON & XML support matching and responses.
   - Supports JSON Schema body matching.
   - Works in both runtime and testing environments.
   - Can be used as decorator and/or via context managers.
   - Supports real networking mode with optional traffic filtering.
   - Map/filter mocks easily for generic or custom mock expectations.
   - Custom user-defined mock matcher functions.
   - Simulated raised error exceptions.
   - Network delay simulation (only available for aiohttp).
   - Pluggable and hackable API.
   - Customizable HTTP traffic mock interceptor engine.
   - Supports third-party mocking engines, such as mocket.
   - Fits good for painless test doubles.
   - Does not support WebSocket traffic mocking.
   - Works with Python +2.7 and +3.0 (including PyPy).
   - Dependency-less: just 2 small dependencies for JSONSchema and XML tree
     comparison.
 .
 This is the common documentation package.
